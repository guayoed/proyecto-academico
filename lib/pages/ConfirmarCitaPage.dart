import 'package:proyecto_academico/pages/InicioPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:proyecto_academico/pages/ServiciosPage.dart';

class ConfirmarCitaPage extends StatefulWidget{
  @override
  State<ConfirmarCitaPage> createState() => _ConfirmarCitaPageState();
}

class Servicio {
  Servicio({
    required this.titulo,
    required this.descripcion,
    required this.dia,
    required this.hora,
    required this.staff
  });
  final String titulo;
  final String descripcion;
  final DateTime dia;
  final TimeOfDay hora;
  final String staff;
}

class _ConfirmarCitaPageState extends State<ConfirmarCitaPage> {
  List<Servicio> servicios = [
    Servicio(
        titulo: "Corte de cabello",
        descripcion: "Puede variar en estilo y longitud dependiendo de las preferencias personales y de la moda actual. Corte a gusto del cliente con máquina o tijera.",
        dia: DateTime(2023, 5, 15, 10, 0),
        hora: TimeOfDay(hour: 12, minute: 00),
        staff: "Angela torres."
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            title: Text('Confirmar cita'),
            flexibleSpace: Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color(0xFF3A6186),
                    Color(0xFF89253E),
                  ],
                ),
              ),
            ),
          ),
          body: ListView.builder(
              itemCount: servicios.length,
              itemBuilder: (context, index){
                return Container(
                  margin: const EdgeInsets.all(16.0),
                  padding: const EdgeInsets.all(16.0),
                  decoration: BoxDecoration(
                      color: Colors.grey[100],
                      borderRadius: BorderRadius.circular(8.0)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(servicios[index].titulo.toString(),
                        style: const TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                      Text(servicios[index].descripcion.toString(),
                        style: const TextStyle(
                          fontSize: 15.0,
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 16)),
                      Row(
                        children: [
                          Material(
                            borderRadius: BorderRadius.circular(30),
                            elevation: 5,
                            color: Color(0xFFCC9BA7),
                            child: Container(
                              width: 80,
                              height: 15,
                              child: Center(
                                child: Text(
                                  servicios[index].staff,
                                  style: TextStyle(fontSize: 11, color: Color(0xFF89253E), fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 16)),
                      Row(
                        children: [
                          Material(
                            borderRadius: BorderRadius.circular(30),
                            elevation: 5,
                            color: Color(0xFF97C0B5),
                            child: Container(
                              width: 65,
                              height: 15,
                              child: Center(
                                child: Text(
                                  servicios[index].dia.day.toString() + "/" + servicios[index].dia.month.toString() + "/" + servicios[index].dia.year.toString(),
                                  style: TextStyle(fontSize: 11, color: Color(0xFF258965), fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                          Padding(padding: EdgeInsets.all(16)),
                          Material(
                            borderRadius: BorderRadius.circular(30),
                            elevation: 5,
                            color: Color(0xFFA0AFC0),
                            child: Container(
                              width: 65,
                              height: 15,
                              child: Center(
                                child: Text(
                                  servicios[index].hora.hour.toString() + ":" + servicios[index].hora.minute.toString() + " hrs",
                                  style: TextStyle(fontSize: 11, color: Color(0xFF3A6186), fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.all(16)),
                      Column(
                        children: [
                          Center(
                            child: Column(
                              children: [
                                Padding(padding: EdgeInsets.all(16)),
                                GradientButton(
                                  text: 'Confirmar cita',
                                  gradientColors: [Color(0xFF3A6186), Color(0xFF89253E)],
                                  onPressed: () {
                                    Navigator.push(
                                        context, MaterialPageRoute(builder: (context) => Confirmacion())
                                    );
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                );
              }
          )
    );
  }
}

class Confirmacion extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Confirmacion'),
        flexibleSpace: Container(

          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Color(0xFF3A6186),
                Color(0xFF89253E),
              ],
            ),
          ),
        ),
      ),
      body: Container(
        margin: const EdgeInsets.all(16.0),
        padding: const EdgeInsets.all(16.0),
        decoration: BoxDecoration(
            color: Colors.grey[100],
            borderRadius: BorderRadius.circular(8.0)
        ),
        child: Center(
          child: Column(
            children: [
              Text("Se ha agendado su cita exitosamente"),
              Padding(padding: EdgeInsets.only(top: 16)),
              GradientButton(
                text: 'Ir a inicio',
                gradientColors: [Color(0xFF3A6186), Color(0xFF89253E)],
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => InicioPage())
                  );
                },
              ),
            ],
          ),
        ),
      )
    );
  }
}