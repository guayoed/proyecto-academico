import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:proyecto_academico/pages/AgendarCitaPage.dart';


class ServiciosPage extends StatefulWidget {
  @override
  State<ServiciosPage> createState() => _ServiciosPageState();
}

class Staff {
  String name;
  String imagePath;

  Staff({required this.name, required this.imagePath});
}

class Servicio {
  Servicio({
    required this.titulo,
    required this.descripcion,
    required this.tiempo,
    required this.precio,

    required this.staffList,
  });
  final String titulo;
  final String descripcion;
  final int tiempo;
  final double precio;

  final List<Staff> staffList;
  Staff? selectedStaff;
}

class ImageRadioModel {
  final String imagePath;
  final bool isSelected;

  ImageRadioModel({required this.imagePath, this.isSelected = false});
}

class _ServiciosPageState extends State<ServiciosPage> {
  List<Servicio> servicios = [
    Servicio(
        titulo: "Corte de cabello",
        descripcion: "Puede variar en estilo y longitud dependiendo de las preferencias personales y de la moda actual. Corte a gusto del cliente con máquina o tijera.",
        tiempo: 45,
        precio: 75.0,

        staffList: [Staff(name: 'Angela torres', imagePath: 'assets/1.jpg'),
          Staff(name: 'Jimena Perez', imagePath: 'assets/2.jpg'),
          Staff(name: 'Elizabeth Rodriguez', imagePath: 'assets/3.jpg'),]
    ),
    Servicio(
        titulo: "Tinte de cabello",
        descripcion: "Distintos tipos de colores, degradados, uniformes o decolorados.",
        tiempo: 90,
        precio: 350.0,
        staffList: [Staff(name: 'José Saturnino', imagePath: 'assets/4.jpg'),
          Staff(name: 'Angela Agilar', imagePath: 'assets/5.jpg'),
          Staff(name: 'Saúl Hernández', imagePath: 'assets/5.jpg'),]
    )
  ];

  List<ImageRadioModel> imageRadioList = [
    ImageRadioModel(imagePath: 'assets/image1.jpg'),
    ImageRadioModel(imagePath: 'assets/image2.png'),
    ImageRadioModel(imagePath: 'assets/image3.png'),
  ];

  ImageRadioModel? selectedImageRadio;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Servicios'),
            flexibleSpace: Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color(0xFF3A6186),
                    Color(0xFF89253E),
                  ],
                ),
              ),
            ),
          ),
          body: ListView.builder(
              itemCount: servicios.length,
              itemBuilder: (context, index){
                Servicio servicio = servicios[index];
                return Container(
                  margin: const EdgeInsets.all(16.0),
                  padding: const EdgeInsets.all(16.0),
                  decoration: BoxDecoration(
                      color: Colors.grey[100],
                      borderRadius: BorderRadius.circular(8.0)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(servicios[index].titulo.toString(),
                        style: const TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                      Text(servicios[index].descripcion.toString(),
                        style: const TextStyle(
                          fontSize: 15.0,
                        ),
                      ),
                      Row(
                        children: [
                          Material(
                            borderRadius: BorderRadius.circular(30),
                            elevation: 5,
                            color: Color(0xFF97C0B5),
                            child: Container(
                              width: 65,
                              height: 15,
                              child: Center(
                                child: Text(
                                  '\$${servicios[index].precio.toStringAsFixed(2)}',
                                  style: TextStyle(fontSize: 11, color: Color(0xFF258965), fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                          Padding(padding: EdgeInsets.all(16)),
                          Material(
                            borderRadius: BorderRadius.circular(30),
                            elevation: 5,
                            color: Color(0xFFA0AFC0),
                            child: Container(
                              width: 65,
                              height: 15,
                              child: Center(
                                child: Text(
                                  servicios[index].tiempo.toString()+" mins",
                                  style: TextStyle(fontSize: 11, color: Color(0xFF3A6186), fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.all(16)),
                      Text("Elegir un staff",
                        style: const TextStyle(fontSize: 10, fontWeight: FontWeight.w100
                        ),
                      ),
                      Column(
                        children: [
                          Center(
                            child: Column(
                              children: [
                                ListView.builder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: servicio.staffList.length,
                                  itemBuilder: (context, staffIndex) {
                                    Staff staff = servicio.staffList[staffIndex];
                                    return RadioListTile(
                                      title: Text(staff.name),
                                      secondary: Image.asset('${staff.imagePath}'),
                                      value: staff,
                                      groupValue: servicio.selectedStaff,
                                      onChanged: (selectedStaff){
                                        setState(() {
                                          servicio.selectedStaff = selectedStaff as Staff?;
                                        });
                                      },
                                    );
                                  },
                                ),

                                Padding(padding: EdgeInsets.all(16)),
                                GradientButton(
                                  text: 'Confirmar cita',
                                  gradientColors: [Color(0xFF3A6186), Color(0xFF89253E)],
                                  onPressed: () {
                                    Navigator.push(
                                        context, MaterialPageRoute(builder: (context) => AgendarCitaPage())
                                    );
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                );
              }
          )
      ),
    );
  }
}

class GradientButton extends StatelessWidget {
  final String text;
  final List<Color> gradientColors;
  final VoidCallback onPressed;

  GradientButton({required this.text, required this.gradientColors, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: gradientColors,
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          primary: Colors.transparent,
          elevation: 0.0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
        ),
        child: Text(text),
      ),
    );
  }
}

