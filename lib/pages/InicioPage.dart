import 'package:flutter/material.dart';
import 'package:proyectoagenda/pages/AgendarCitaPage.dart';
import 'package:intl/intl.dart';
import 'package:proyectoagenda/pages/ServiciosPage.dart';

class Inicio {
  Inicio({
    required this.servicio,
    required this.servidor,
    required this.fecha,
    required this.hora,
    required this.costo,
    required this.descripcion,
  });

  final String servicio;
  final String servidor;
  final DateTime fecha;
  final DateTime hora;
  final double costo;
  final String descripcion;
}

final String proximaCitaFecha = '30 de mayo de 2023';
final String hora = '3:00 p.m.';
final String servicio = 'Corte de pelo';
final String descripcion = "Puede variar en estilo y longitud dependiendo de las preferencias personales y de la moda actual. Corte a gusto del cliente con máquina o tijera.";
final String servidor = 'Ana María';
final double costo = 50.0;

class InicioPage extends StatelessWidget {
  final List<Inicio> ultimos_servicios = [
    Inicio(
      servicio: 'Corte de pelo',
      descripcion: "Puede variar en estilo y longitud dependiendo de las preferencias personales y de la moda actual. Corte a gusto del cliente con máquina o tijera.",
      servidor: 'Ana María',
      fecha: DateTime(2023, 5, 15),
      hora: DateTime(2023, 5, 15, 10),
      costo: 50.0,
    ),
    Inicio(
      servicio: 'Tinte de cabello',
      servidor: 'Erick Filiberto',
      descripcion: "Distintos tipos de colores, degradados, uniformes o decolorados.",
      fecha: DateTime(2023, 5, 15),
      hora: DateTime(2023, 5, 15, 10, 00),
      costo: 30.0,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Inicio'),
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Color(0xFF3A6186),
                Color(0x3589253E),
              ],
            ),
          ),
        ),
        toolbarHeight: 100,
      ),
      body: ListView(
        padding: const EdgeInsets.all(16.0),
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 16.0),
            padding: const EdgeInsets.all(16.0),
            decoration: BoxDecoration(
              color: Colors.grey[100],
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Próxima cita',
                      style: const TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                    Text(
                      proximaCitaFecha,
                      style: const TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                Padding(padding: EdgeInsets.all(10)),
                Text(
                  servicio,
                  style: const TextStyle(
                    fontSize: 20.0,
                  ),
                ),
                Text(
                  descripcion,
                  style: const TextStyle(
                    fontSize: 14.0,
                  ),
                ),
                Row(
                  children: [
                    Material(
                      borderRadius: BorderRadius.circular(30),
                      elevation: 5,
                      color: Color(0xFFCC9BA7),
                      child: Container(
                        width: 105,
                        height: 15,
                        child: Center(
                          child: Text(
                            servidor,
                            style: TextStyle(
                              fontSize: 11,
                              color: Color(0xFF89253E),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.all(16)),
                    Material(
                      borderRadius: BorderRadius.circular(30),
                      elevation: 5,
                      color: Color(0xFFA0AFC0),
                      child: Container(
                        width: 65,
                        height: 15,
                        child: Center(
                          child: Text(
                            hora,
                            style: TextStyle(
                              fontSize: 11,
                              color: Color(0xFF3A6186),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Padding(
              padding: const EdgeInsets.only(bottom: 12.0, top: 32.0),
              child: Text(
                'Últimas citas',
                style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          for (var inicio in ultimos_servicios)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  inicio.servicio,
                  style: const TextStyle(
                    fontSize: 15.0,
                  ),
                ),
                Text(
                  inicio.hora.toString(),
                  style: const TextStyle(
                    fontSize: 10.0,
                  ),
                ),
                Row(
                  children: [
                    Icon(Icons.person), // Icono de persona
                    Material(
                      borderRadius: BorderRadius.circular(30),
                      elevation: 5,
                      color: Color(0xFFCC9BA7),
                      child: Container(
                        width: 105,
                        height: 15,
                        child: Center(
                          child: Text(
                            inicio.servidor,
                            style: TextStyle(
                              fontSize: 11,
                              color: Color(0xFF89253E),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.all(16)),
                    Material(
                      borderRadius: BorderRadius.circular(30),
                      elevation: 5,
                      color: Color(0xFF97C0B5),
                      child: Container(
                        width: 65,
                        height: 15,
                        child: Center(
                          child: Text(
                            '\$${inicio.costo.toStringAsFixed(2)}',
                            style: TextStyle(
                              fontSize: 11,
                              color: Color(0xFF258965),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(padding: EdgeInsets.all(16)),
              ],
            ),
        ],
      ),
    );
  }
}
