import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:proyecto_academico/pages/InicioPage.dart';
import 'package:proyecto_academico/pages/ServiciosPage.dart';

class AgendaPage extends StatefulWidget {
  @override
  State<AgendaPage> createState() => _AgendaPageState();
}

class Appoinment {
  final String day;
  final String activity;
  final DateTime dateTime;
  final String clientName;
  final double price;

  Appoinment({
    required this.day,
    required this.activity,
    required this.dateTime,
    required this.clientName,
    required this.price,
  });
}

class _AgendaPageState extends State<AgendaPage> {
  final List<Appoinment> appointments = [
    Appoinment(day: 'Cita 20 de Marzo', activity: 'Actividad 1', dateTime: DateTime(2023, 5, 15, 10, 0), clientName: 'John Doe', price: 50.0),
    Appoinment(day: 'Cita 25 de Marzo', activity: 'Actividad 2', dateTime: DateTime(2023, 5, 16, 14, 30), clientName: 'Jane Smith', price: 75.0),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Próximas citas'),
          flexibleSpace: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Color(0xFF3A6186),
                  Color(0xFF89253E),
                ],
              ),
            ),
          ),
        ),
        body: ListView.builder(
            itemCount: appointments.length,
            itemBuilder: (context, index) {
              return Container(
                margin: const EdgeInsets.all(16.0),
                padding: const EdgeInsets.all(16.0),
                decoration: BoxDecoration(
                  color: Colors.grey[100],
                  borderRadius: BorderRadius.circular(8.0)
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                     appointments[index].day,
                      style: const TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    const SizedBox(height: 16.0),
                    Column(
                      children: appointments
                          .map(
                              (appointment) => Container(
                                padding: EdgeInsets.all(8.0),
                                decoration: BoxDecoration(
                                  // border: Border.all(color: Colors.green),
                                  borderRadius: BorderRadius.circular(4.0)
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      appointment.activity,
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold
                                      ),
                                    ),
                                    SizedBox(height: 4.0),
                                    Text(
                                      '${appointment.dateTime.day}/${appointment.dateTime.month}/${appointment.dateTime.year} - ${appointment.dateTime.hour}:${appointment.dateTime.minute}',
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        color: Colors.grey,
                                      ),
                                    ),
                                    SizedBox(height: 8.0),
                                    Row(
                                      mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                      children: [
                                        Material(
                                          borderRadius: BorderRadius.circular(30),
                                          elevation: 5,
                                          color: Color(0xFFCC9BA7),
                                          child: Container(
                                            width: 65,
                                            height: 15,
                                            child: Center(
                                              child: Text(
                                                appointment.clientName,
                                                style: TextStyle(fontSize: 11, color: Color(0xFF89253E), fontWeight: FontWeight.bold),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Material(
                                          borderRadius: BorderRadius.circular(30),
                                          elevation: 5,
                                          color: Color(0xFF97C0B5),
                                          child: Container(
                                            width: 65,
                                            height: 15,
                                            child: Center(
                                              child: Text(
                                                '\$${appointment.price.toStringAsFixed(2)}',
                                                style: TextStyle(fontSize: 11, color: Color(0xFF258965), fontWeight: FontWeight.bold),
                                              ),
                                            ),
                                          ),
                                        ),
                                        // Text(
                                        //   appointment.clientName,
                                        //   style: TextStyle(
                                        //     color: Colors.red
                                        //   ),
                                        // ),
                                        // Text(
                                        //   '\$${appointment.price.toStringAsFixed(2)}',
                                        //   style: TextStyle(
                                        //     color: Colors.green
                                        //   ),
                                        // ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ).toList(),
                    ),
                    SizedBox(height: 16.0),
                    Text(
                      'Total',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 4.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Material(
                          borderRadius: BorderRadius.circular(30),
                          elevation: 5,
                          color: Color(0xFFA0AFC0),
                          child: Container(
                            width: 65,
                            height: 15,
                            child: Center(
                              child: Text(
                                '\$${calculateTotal(appointments[index].price)}.00',
                                style: TextStyle(fontSize: 11, color: Color(0xFF3A6186), fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                        // Text(
                        //   'Total: \$${calculateTotal(appointments[index].price)}',
                        //   style: TextStyle(
                        //     fontWeight: FontWeight.bold
                        //   ),
                        // ),
                        ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(Color(0xFF89253E)),
                            ),
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context){
                                    return AlertDialog(
                                      title: Text('Confirmación de cancelación'),
                                      content: Text('¿Estás seguro que quieres cancelar la cita?'),
                                      actions: [
                                        // TextButton(
                                        //     onPressed: () {
                                        //       Navigator.of(context).pop();
                                        //     },
                                        //     child: Text('Regresar')
                                        // ),
                                        ElevatedButton(
                                          style: ButtonStyle(
                                            backgroundColor: MaterialStateProperty.all<Color>(Color(0xFFFFFFFF)),
                                          ),
                                            onPressed: (){
                                              Navigator.of(context).pop();
                                            },
                                            child: Text('Regresar', style: TextStyle(color: Colors.black),),
                                        ),
                                        // TextButton(
                                        //     onPressed: () {
                                        //       // Codigo para cancelar cita
                                        //       Navigator.of(context).pop();
                                        //     },
                                        //     child: Text('Cancelar')
                                        // ),
                                        ElevatedButton(
                                          style: ButtonStyle(
                                            backgroundColor: MaterialStateProperty.all<Color>(Color(0xFFCC9BA7)),
                                          ),
                                          onPressed: (){
                                            // Codigo para cancelar cita
                                            Navigator.push(
                                                context, MaterialPageRoute(builder: (context) => Cancelacion())
                                            );
                                          },
                                          child: Text('Cancelar', style: TextStyle(color: Color(0xFF89253E)),),
                                        ),
                                      ],
                                    );
                                  },
                                );
                            },
                            child: Text('Cancelar'),

                        ),
                      ],
                    )
                  ],
                ),
              );
            }),
      ),


    );
  }


  double calculateTotal(double price){


    return price * appointments.length;
  }
}


class Cancelacion extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: AppBar(
          title: Text('Confirmacion'),
          flexibleSpace: Container(

            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Color(0xFF3A6186),
                  Color(0xFF89253E),
                ],
              ),
            ),
          ),
        ),
        body: Container(
          margin: const EdgeInsets.all(16.0),
          padding: const EdgeInsets.all(16.0),
          decoration: BoxDecoration(
              color: Colors.grey[100],
              borderRadius: BorderRadius.circular(8.0)
          ),
          child: Center(
            child: Column(
              children: [
                Text("Su cita ha sido cancelada exitosamente"),
                Padding(padding: EdgeInsets.only(top: 16)),
                GradientButton(
                  text: 'Ir a inicio',
                  gradientColors: [Color(0xFF3A6186), Color(0xFF89253E)],
                  onPressed: () {
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context) => InicioPage())
                    );
                  },
                ),
              ],
            ),
          ),
        )
    );
  }
}

