import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PromocionesPage extends StatefulWidget {
  @override
  State<PromocionesPage> createState() => _PromocionesPageState();
}

class Promo {
  Promo({
    required this.titulo,
    required this.descripcion,
    required this.lugar
  });
  final String titulo;
  final String descripcion;
  final String lugar;
}

class _PromocionesPageState extends State<PromocionesPage> {
  List<Promo> promociones = [
    Promo(
        titulo: "Corte de cabello gratis",
        descripcion: "Por el dia internacional del peluquero ell dia de hoy todos los cortes sencillos tienen un 100% de descuento",
        lugar: "Barbería Capital"
    ),
    Promo(
        titulo: "Corte de cabello gratis",
        descripcion: "Por el dia internacional del peluquero ell dia de hoy todos los cortes sencillos tienen un 100% de descuento",
        lugar: "Barbería Capital"
    ),
    Promo(
        titulo: "Corte de cabello gratis",
        descripcion: "Por el dia internacional del peluquero ell dia de hoy todos los cortes sencillos tienen un 100% de descuento",
        lugar: "Barbería Capital"
    ),
    Promo(
        titulo: "Corte de cabello gratis",
        descripcion: "Por el dia internacional del peluquero ell dia de hoy todos los cortes sencillos tienen un 100% de descuento",
        lugar: "Barbería Capital"
    ),
    Promo(
        titulo: "Corte de cabello gratis",
        descripcion: "Por el dia internacional del peluquero ell dia de hoy todos los cortes sencillos tienen un 100% de descuento",
        lugar: "Barbería Capital")
  ];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Promociones'),
            flexibleSpace: Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color(0xFF3A6186),
                    Color(0xFF89253E),
                  ],
                ),
              ),
            ),
          ),
          body: ListView.builder(
              itemCount: promociones.length,
              itemBuilder: (context, index){
                return Container(
                  margin: const EdgeInsets.all(16.0),
                  padding: const EdgeInsets.all(16.0),
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(8.0)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(promociones[index].titulo.toString(),
                        style: const TextStyle(
                          fontSize: 20.0,
                            fontWeight: FontWeight.w500
                        ),
                      ),
                      Divider(),
                      Text(promociones[index].descripcion.toString(),
                        style: const TextStyle(
                          fontSize: 15.0,
                        ),
                      ),
                      Padding(padding: EdgeInsets.all(4)),
                      Text(promociones[index].lugar.toString(),
                        style: const TextStyle(
                          fontSize: 15.0,
                            fontWeight: FontWeight.w500
                        ),
                      ),
                      Column(
                        children: [
                          Center(
                            child: Align(
                              alignment: Alignment.bottomRight,
                                child:
                              GradientButtonProm(
                                  text: 'Ir  ',
                                  gradientColors: [Color(0xFF3A6186), Color(0xFF89253E)],
                                  onPressed: () {
                                  },
                                ),

                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                );
              }
          )
      ),
    );
  }
}
class GradientButtonProm extends StatelessWidget {
  final String text;
  final List<Color> gradientColors;
  final VoidCallback onPressed;

  GradientButtonProm({required this.text, required this.gradientColors, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: gradientColors,
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: ElevatedButton(
        onPressed: onPressed,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(text),
              SizedBox(
              ),
              Icon(
                Icons.arrow_circle_right_outlined,
              ),
            ],
          ),
        style: ElevatedButton.styleFrom(
          primary: Colors.transparent,
          elevation: 0.0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
        ),
      ),
    );
  }
}